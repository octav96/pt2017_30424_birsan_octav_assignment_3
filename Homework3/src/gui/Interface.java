package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

//import com.mysql.fabric.xmlrpc.Client;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.List;
import java.util.*;

import model.Client;
import model.Order;
import model.Product;

public class Interface extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Interface() {
		setTitle("Database");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1173, 780);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		DefaultTableModel model1=new DefaultTableModel();
		
		
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(80, 412, 373, 23);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("!!NOTIFICATION!!");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(174, 392, 174, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblClientInformation = new JLabel("CLIENT INFORMATION");
		lblClientInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblClientInformation.setBounds(196, 66, 152, 14);
		contentPane.add(lblClientInformation);
		
		textField_1 = new JTextField();
		textField_1.setBounds(57, 82, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(174, 82, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(283, 82, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(406, 82, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblName = new JLabel("NAME");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setBounds(80, 102, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblAge = new JLabel("AGE");
		lblAge.setHorizontalAlignment(SwingConstants.CENTER);
		lblAge.setBounds(196, 102, 46, 14);
		contentPane.add(lblAge);
		
		JLabel lblShoeSize = new JLabel("SHOE SIZE");
		lblShoeSize.setHorizontalAlignment(SwingConstants.CENTER);
		lblShoeSize.setBounds(274, 102, 110, 14);
		contentPane.add(lblShoeSize);
		
		JLabel lblCountry = new JLabel("COUNTRY");
		lblCountry.setHorizontalAlignment(SwingConstants.CENTER);
		lblCountry.setBounds(412, 102, 80, 14);
		contentPane.add(lblCountry);
		
		JLabel lblNewLabel_1 = new JLabel("PRODUCT INFORMATION");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(196, 193, 152, 14);
		contentPane.add(lblNewLabel_1);
		
		textField_5 = new JTextField();
		textField_5.setBounds(57, 208, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(174, 208, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(283, 208, 86, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(412, 208, 86, 20);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("PRICE");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(80, 230, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblName_1 = new JLabel("NAME");
		lblName_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblName_1.setBounds(196, 230, 46, 14);
		contentPane.add(lblName_1);
		
		JLabel lblNewLabel_3 = new JLabel("QUANTITY");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(283, 230, 86, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("PRODUCER");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(412, 230, 80, 14);
		contentPane.add(lblNewLabel_4);
		
		textField_9 = new JTextField();
		textField_9.setBounds(104, 331, 86, 20);
		contentPane.add(textField_9);
		textField_9.setColumns(10);
		
		textField_10 = new JTextField();
		textField_10.setBounds(221, 331, 86, 20);
		contentPane.add(textField_10);
		textField_10.setColumns(10);
		
		JLabel lblOrderInformation = new JLabel("ORDER INFORMATION");
		lblOrderInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrderInformation.setBounds(184, 317, 152, 14);
		contentPane.add(lblOrderInformation);
		
		textField_11 = new JTextField();
		textField_11.setBounds(339, 331, 86, 20);
		contentPane.add(textField_11);
		textField_11.setColumns(10);
		
		JLabel lblQuantity = new JLabel("QUANTITY");
		lblQuantity.setHorizontalAlignment(SwingConstants.CENTER);
		lblQuantity.setBounds(104, 353, 86, 14);
		contentPane.add(lblQuantity);
		
		JLabel lblNewLabel_5 = new JLabel("CLIENT ID");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5.setBounds(221, 353, 86, 14);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("PRODUCT ID");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_6.setBounds(339, 353, 86, 14);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblClient = new JLabel("CLIENT");
		lblClient.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblClient.setHorizontalAlignment(SwingConstants.CENTER);
		lblClient.setBounds(249, 11, 46, 23);
		contentPane.add(lblClient);
		
		JLabel lblProduct = new JLabel("PRODUCT");
		lblProduct.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProduct.setHorizontalAlignment(SwingConstants.CENTER);
		lblProduct.setBounds(239, 134, 68, 14);
		contentPane.add(lblProduct);
		
		JLabel lblNewLabel_7 = new JLabel("ORDER");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_7.setBounds(239, 258, 68, 14);
		contentPane.add(lblNewLabel_7);
		Object[] columns1={"idClient","clientName","clientAge","clientShoeSize","clientCountry"};
		//DefaultTableModel model1=new DefaultTableModel();
		model1.setColumnIdentifiers(columns1);
		Font font = new Font("",1,10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(627, 24, 496, 92);
		contentPane.add(scrollPane);
		
		JTable clientTable= new JTable();
		scrollPane.setViewportView(clientTable);
		clientTable.setModel(model1);
		clientTable.setVisible(true);
		clientTable.setBackground(Color.green);
		clientTable.setForeground(Color.BLACK);
		clientTable.setFont(font);
	//	clientTable.setRowHeight(20);
		clientTable.setAutoResizeMode(MAXIMIZED_BOTH);
		Object[] columns2 ={"idProduct","productPrice","productName","productQuantity","productProducer"};
		DefaultTableModel model2=new DefaultTableModel();
		model2.setColumnIdentifiers(columns2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(625, 134, 498, 110);
		contentPane.add(scrollPane_1);
		
		JTable productTable=new JTable();
		scrollPane_1.setViewportView(productTable);
		productTable.setModel(model2);
		productTable.setVisible(true);
		productTable.setBackground(Color.YELLOW);
		productTable.setForeground(Color.BLACK);
		//Font font2 = new Font("",1,20);
		//productTable.setFont(font);
		productTable.setRowHeight(20);
		DefaultTableModel model3=new DefaultTableModel();
		Object[] columns3 = {"idOrder","orderQuantity","idClient","idProduct"};
		model3.setColumnIdentifiers(columns3);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(627, 258, 496, 109);
		contentPane.add(scrollPane_2);
		//contentPane.add(productTable);
		//contentPane.add(clientTable);
		JTable orderTable= new JTable();
		scrollPane_2.setViewportView(orderTable);
		orderTable.setModel(model3);
		//orderTable.setModel(model1);
		orderTable.setVisible(true);
		orderTable.setBackground(Color.PINK);
		orderTable.setForeground(Color.BLACK);
		orderTable.setFont(font);
		orderTable.setRowHeight(20);
		orderTable.setVisible(true);

		Object[] client=new Object[5];
		JButton btnNewButton = new JButton("ADD CLIENT");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClientDAO cldao=new ClientDAO();
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				String s1=textField_1.getText(); //name
				String s2=textField_2.getText(); //age
				String s3=textField_3.getText();//shoe size
				String s4=textField_4.getText();//country
				client[0]=s1;
				client[1]=s2;
				client[2]=s3;
				client[3]=s4;
				if(Integer.parseInt(s2)>150 || Integer.parseInt(s2)<18){
					textField.setText("CLIENT AGE NOT BETWEEN ACCEPTABLE LIMITS");
					return;
				}
				if(Integer.parseInt(s3)>70 || Integer.parseInt(s3)<5){
					textField.setText("CLIENT SHOE SIZE NOT BETWEEN ACCEPTABLE LIMITS");
					return;
				}
				Client c = new Client(s1,Integer.parseInt(s2),Integer.parseInt(s3),s4);
				int verif=cldao.insert(c);
				//model1.addRow(client);
				if(verif==-1)
					textField.setText("COULD NOT INSERT CLIENT!");
				else
					textField.setText("CLIENT ADDED SUCCESSFULLY!");
				ArrayList<Client> clients= (ArrayList<Client>)cldao.findAll();
				for(int i=0;i<clients.size();i++){
					client[0]=clients.get(i).getId();
					client[1]=clients.get(i).getName();
					client[2]=clients.get(i).getAge();
					client[3]=clients.get(i).getShoeSize();
					client[4]=clients.get(i).getCountry();
					model1.addRow(client);
				}
			}
		});
		btnNewButton.setBounds(10, 36, 121, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("UPDATE CLIENT");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO cldao=new ClientDAO();
				
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				
				String s0=textField_12.getText();
				String s1=textField_1.getText(); //name
				String s2=textField_2.getText(); //age
				String s3=textField_3.getText();//shoe size
				String s4=textField_4.getText();//country
				Client c=cldao.updateClient(Integer.parseInt(s0), s1, Integer.parseInt(s2), Integer.parseInt(s3), s4);
				ArrayList<Client> clients= (ArrayList<Client>)cldao.findAll();
				if(c==null)
					textField.setText("COULD NOT UPDATE CLIENT!");
				else
					textField.setText("CLIENT UPDATED SUCCESFULLY!");
				
				for(int i=0;i<clients.size();i++){
					client[0]=clients.get(i).getId();
					client[1]=clients.get(i).getName();
					client[2]=clients.get(i).getAge();
					client[3]=clients.get(i).getShoeSize();
					client[4]=clients.get(i).getCountry();
					model1.addRow(client);
				}
				
			}
			
		});
		btnNewButton_1.setBounds(141, 36, 132, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("FIND CLIENT");
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO cldao=new ClientDAO();
				
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				
				String s0=textField_12.getText();
				Client c=cldao.findById(Integer.parseInt(s0));
				if(c==null){
					textField.setText("CLIENT NOT FOUND!");
				}else{
					textField.setText("CLIENT SUCCESSFULLY FOUND!");
					client[0]=c.getId();
					client[1]=c.getName();
					client[2]=c.getAge();
					client[3]=c.getShoeSize();
					client[4]=c.getCountry();
					model1.addRow(client);
					
				}
			}
		});
		btnNewButton_2.setBounds(283, 36, 119, 23);
		contentPane.add(btnNewButton_2);
		JButton btnNewButton_3 = new JButton("DELETE CLIENT");
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO cldao=new ClientDAO();
				OrderDAO ordao=new OrderDAO();
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				ArrayList<Order> verifOrder=(ArrayList<Order>) ordao.findAll();
				String s0=textField_12.getText();
				int sem=1;
				
				for(int i=0;i<verifOrder.size();i++){
					if(verifOrder.get(i).getClientId()==Integer.parseInt(s0))
						sem=0;
				}
				if(sem==0)
					textField.setText("COULD NOT DELETE CLIENT!(EXISTS IN ORDER)");
				else{
				Client c=cldao.deleteClient(Integer.parseInt(s0));
				
				if(c==null){
					textField.setText("CLIENT DOES NOT EXIST!");
				}else{
					textField.setText("CLIENT DELETED SUCCESSFULLY!");
				}
				
				}
				ArrayList<Client> clients= (ArrayList<Client>)cldao.findAll();
				for(int i=0;i<clients.size();i++){
					client[0]=clients.get(i).getId();
					client[1]=clients.get(i).getName();
					client[2]=clients.get(i).getAge();
					client[3]=clients.get(i).getShoeSize();
					client[4]=clients.get(i).getCountry();
					model1.addRow(client);
				
			}
			}
		});
		btnNewButton_3.setBounds(412, 36, 139, 23);
		contentPane.add(btnNewButton_3);
		Object[] product=new Object[5];
		JButton btnNewButton_4 = new JButton("ADD PRODUCT");
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO prdao=new ProductDAO();
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				String s1=textField_5.getText(); //price
				String s2=textField_6.getText(); //name
				String s3=textField_7.getText();//quantity
				String s4=textField_8.getText();//producer
				if(Integer.parseInt(s1)<0){
					textField.setText("THE PRODUCT PRICE CAN NOT BE NEGATIVE!");
					return;
				}
				Product p = new Product(Double.parseDouble(s1),s2,Integer.parseInt(s3),s4);
				int verif=prdao.insert(p);
				//model1.addRow(client);
				ArrayList<Product> products= (ArrayList<Product>)prdao.findAll();
				//System.out.println( products.get(products.size()-1).getPrice() + " " + s1);
				if(verif==-1)
					textField.setText("COULD NOT INSERT PRODUCT!");
				else
					textField.setText("PRODUCT ADDED SUCCESSFULLY!");
				//ArrayList<Product> products= (ArrayList<Product>)prdao.findAll();
				for(int i=0;i<products.size();i++){
					product[0]=products.get(i).getId();
					product[1]=products.get(i).getPrice();
					product[2]=products.get(i).getName();
					product[3]=products.get(i).getQuantity();
					product[4]=products.get(i).getProducer();
					model2.addRow(product);
				}
		
			}
		});
		btnNewButton_4.setBounds(10, 159, 121, 23);
		contentPane.add(btnNewButton_4);
		

		JButton btnNewButton_5 = new JButton("UPDATE PRODUCT");
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO prdao=new ProductDAO();
				
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				
				String s0=textField_13.getText();
				String s1=textField_5.getText(); //price
				String s2=textField_6.getText(); //name
				String s3=textField_7.getText();//quantity
				String s4=textField_8.getText();//producer
				Product p=prdao.updateProduct(Integer.parseInt(s0), Double.parseDouble(s1),s2, Integer.parseInt(s3), s4);
				ArrayList<Product> products= (ArrayList<Product>)prdao.findAll();
				if(p==null)
					textField.setText("COULD NOT UPDATE PRODUCT!");
				else
					textField.setText("PRODUCT UPDATED SUCCESFULLY!");
				
				for(int i=0;i<products.size();i++){
					product[0]=products.get(i).getId();
					product[1]=products.get(i).getPrice();
					product[2]=products.get(i).getName();
					product[3]=products.get(i).getQuantity();
					product[4]=products.get(i).getProducer();
					model2.addRow(product);
				}
			}
			
		});
		
		btnNewButton_5.setBounds(141, 159, 132, 23);
		contentPane.add(btnNewButton_5);
		
		
		JButton btnNewButton_6 = new JButton("FIND PRODUCT");
		btnNewButton_6.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO prdao=new ProductDAO();
				
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				
				String s0=textField_13.getText();
				Product p=prdao.findById(Integer.parseInt(s0));
				if(p==null){
					textField.setText("PRODUCT NOT FOUND!");
				}else{
					textField.setText("PRODUCT SUCCESSFULLY FOUND!");
					product[0]=p.getId();
					product[1]=p.getPrice();
					product[2]=p.getName();
					product[3]=p.getQuantity();
					product[4]=p.getProducer();
					model2.addRow(product);
					
				}
			}
		});
		btnNewButton_6.setBounds(292, 159, 110, 23);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("DELETE PRODUCT");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO prdao=new ProductDAO();
				OrderDAO ordao=new OrderDAO();
				ArrayList<Order> verifOrder=(ArrayList<Order>) ordao.findAll();
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				String s0=textField_13.getText();
				int sem=1;
				
				for(int i=0;i<verifOrder.size();i++){
					if(verifOrder.get(i).getProductId()==Integer.parseInt(s0))
						sem=0;
				}
				if(sem==0){
					textField.setText("CAN NOT DELETE PRODUCT!(EXISTS IN ORDER)");
				}else{
				Product p=prdao.deleteProduct(Integer.parseInt(s0));
				
				if(p==null){
					textField.setText("PRODUCT DOES NOT EXIST!");
		
				}else{
					textField.setText("PRODUCT DELETED SUCCESSFULLY!");
				}
				
			}
				ArrayList<Product> products= (ArrayList<Product>)prdao.findAll();
				for(int i=0;i<products.size();i++){
					product[0]=products.get(i).getId();
					product[1]=products.get(i).getPrice();
					product[2]=products.get(i).getName();
					product[3]=products.get(i).getQuantity();
					product[4]=products.get(i).getProducer();
					model2.addRow(product);
				}
			}
		});
		
		btnNewButton_7.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_7.setBounds(412, 159, 139, 23);
		contentPane.add(btnNewButton_7);
		Object[] order=new Object[4];
		JButton btnNewButton_8 = new JButton("ADD ORDER");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO ordao=new OrderDAO();
				ProductDAO prdao = new ProductDAO();
				for(int i=model3.getRowCount()-1;i>=0;i--)
					model3.removeRow(i);
				String s1=textField_9.getText(); //quantity
				String s2=textField_10.getText(); //clientID
				String s3=textField_11.getText();//productID
				Order o = new Order(Integer.parseInt(s1),Integer.parseInt(s2),Integer.parseInt(s3));
				Product p=prdao.findById(Integer.parseInt(s3));
				if(p.getQuantity()<Integer.parseInt(s1)){
					textField.setText("CAN NOT ADD ORDER!(UNDERSTOCK)");
				}else{
				int verif=ordao.insert(o);
				prdao.updateProduct(p.getId(), p.getPrice(), p.getName(), p.getQuantity()-Integer.parseInt(s1), p.getProducer());
				//model1.addRow(client);
				ArrayList<Order> orders= (ArrayList<Order>)ordao.findAll();
				//System.out.println( products.get(products.size()-1).getPrice() + " " + s1);
				if(verif==-1)
					textField.setText("COULD NOT INSERT ORDER!");
				else
					textField.setText("ORDER ADDED SUCCESSFULLY!");
				//ArrayList<Product> products= (ArrayList<Product>)prdao.findAll();
				for(int i=0;i<orders.size();i++){
					order[0]=orders.get(i).getId();
					order[1]=orders.get(i).getQuantity();
					order[2]=orders.get(i).getClientId();
					order[3]=orders.get(i).getProductId();
					model3.addRow(order);
				}
				}
			}
		});
		
		btnNewButton_8.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_8.setBounds(10, 283, 121, 23);
		
		JButton btnNewButton_9 = new JButton("UPDATE ORDER");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO ordao=new OrderDAO();
				
				for(int i=model3.getRowCount()-1;i>=0;i--)
					model3.removeRow(i);
				
				String s0=textField_14.getText();
				String s1=textField_9.getText(); //quantity
				String s2=textField_10.getText(); //clientID
				String s3=textField_11.getText();//productID
				Order o=ordao.updateOrder(Integer.parseInt(s0), Integer.parseInt(s1),Integer.parseInt(s2), Integer.parseInt(s3));
				ArrayList<Order> orders= (ArrayList<Order>)ordao.findAll();
				if(o==null)
					textField.setText("COULD NOT UPDATE ORDER!");
				else
					textField.setText("ORDER UPDATED SUCCESFULLY!");
				
				for(int i=0;i<orders.size();i++){
					order[0]=orders.get(i).getId();
					order[1]=orders.get(i).getQuantity();
					order[2]=orders.get(i).getClientId();
					order[3]=orders.get(i).getProductId();
					model3.addRow(order);
				}
			}
			
		});
		
		btnNewButton_9.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_9.setBounds(141, 283, 132, 23);
		contentPane.add(btnNewButton_9);
		

		JButton btnNewButton_10 = new JButton("FIND ORDER");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO ordao=new OrderDAO();
				
				for(int i=model3.getRowCount()-1;i>=0;i--)
					model3.removeRow(i);
				
				String s0=textField_14.getText();
				Order o=ordao.findById(Integer.parseInt(s0));
				if(o==null){
					textField.setText("ORDER NOT FOUND!");
				}else{
					textField.setText("ORDER SUCCESSFULLY FOUND!");
					order[0]=o.getId();
					order[1]=o.getQuantity();
					order[2]=o.getClientId();
					order[3]=o.getProductId();
					model3.addRow(order);
					
				}
			}
		});
		
		btnNewButton_10.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_10.setBounds(292, 283, 110, 23);
		contentPane.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("DELETE ORDER");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO ordao=new OrderDAO();
				
				for(int i=model3.getRowCount()-1;i>=0;i--)
					model3.removeRow(i);
				String s0=textField_14.getText();
				Order o=ordao.deleteOrder(Integer.parseInt(s0));
				ArrayList<Order> orders= (ArrayList<Order>)ordao.findAll();
				if(o==null){
					textField.setText("ORDER DOES NOT EXIST!");
		
				}else{
					textField.setText("ORDER DELETED SUCCESSFULLY!");
				}
				for(int i=0;i<orders.size();i++){
					order[0]=orders.get(i).getId();
					order[1]=orders.get(i).getQuantity();
					order[2]=orders.get(i).getClientId();
					order[3]=orders.get(i).getProductId();
					model3.addRow(order);
				}
			}
		});
		
		btnNewButton_11.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_11.setBounds(412, 283, 139, 23);
		contentPane.add(btnNewButton_11);
		
		
		contentPane.add(btnNewButton_8);
		textField_12 = new JTextField();
		textField_12.setBounds(567, 36, 38, 20);
		contentPane.add(textField_12);
		textField_12.setColumns(10);
		
		JLabel lblId = new JLabel("ID");
		lblId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblId.setBounds(546, 56, 46, 14);
		contentPane.add(lblId);
		
		JLabel label = new JLabel("ID");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(546, 179, 46, 14);
		contentPane.add(label);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(567, 159, 38, 20);
		contentPane.add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(567, 283, 38, 20);
		contentPane.add(textField_14);
		
		JLabel label_1 = new JLabel("ID");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(546, 303, 46, 14);
		contentPane.add(label_1);
		
		JButton btnNewButton_12 = new JButton("REFRESH");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO cldao=new ClientDAO();
				ProductDAO prdao=new ProductDAO();
				OrderDAO ordao=new OrderDAO();
				ArrayList<Client> allClients = (ArrayList<Client>) cldao.findAll();
				ArrayList<Product> allProducts = (ArrayList<Product>) prdao.findAll();
				ArrayList<Order> allOrders = (ArrayList<Order>) ordao.findAll();
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				for(int i=model3.getRowCount()-1;i>=0;i--)
					model3.removeRow(i);
				for(int i=0;i<allClients.size();i++){
					client[0]=allClients.get(i).getId();
					client[1]=allClients.get(i).getName();
					client[2]=allClients.get(i).getAge();
					client[3]=allClients.get(i).getShoeSize();
					client[4]=allClients.get(i).getCountry();
					model1.addRow(client);
				}
				for(int i=0;i<allProducts.size();i++){
					product[0]=allProducts.get(i).getId();
					product[1]=allProducts.get(i).getPrice();
					product[2]=allProducts.get(i).getName();
					product[3]=allProducts.get(i).getQuantity();
					product[4]=allProducts.get(i).getProducer();
					model2.addRow(product);
				}
				for(int i=0;i<allOrders.size();i++){
					order[0]=allOrders.get(i).getId();
					order[1]=allOrders.get(i).getQuantity();
					order[2]=allOrders.get(i).getClientId();
					order[3]=allOrders.get(i).getProductId();
					model3.addRow(order);
				}
				
		
			}
		});
		
		btnNewButton_12.setBounds(767, 390, 197, 23);
		contentPane.add(btnNewButton_12);
		
		JLabel lblFilterClients = new JLabel("FILTER CLIENTS");
		lblFilterClients.setHorizontalAlignment(SwingConstants.CENTER);
		lblFilterClients.setBounds(489, 480, 139, 14);
		contentPane.add(lblFilterClients);
		
		JButton btnAgeFilter = new JButton("AGE FILTER");
		btnAgeFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				ClientDAO cldao=new ClientDAO();
				for(int i=model1.getRowCount()-1;i>=0;i--)
					model1.removeRow(i);
				String s0=textField_15.getText();
				ArrayList<Client> clients=(ArrayList<Client>) cldao.findAll();
				for(int i=0;i<clients.size();i++){
					if(clients.get(i).getAge()>Integer.parseInt(s0)){
						client[0]=clients.get(i).getId();
						client[1]=clients.get(i).getName();
						client[2]=clients.get(i).getAge();
						client[3]=clients.get(i).getShoeSize();
						client[4]=clients.get(i).getCountry();
						model1.addRow(client);
					}
				}
			}
		});
		btnAgeFilter.setBounds(449, 505, 98, 23);
		contentPane.add(btnAgeFilter);
		
		textField_15 = new JTextField();
		textField_15.setBounds(567, 506, 86, 20);
		contentPane.add(textField_15);
		textField_15.setColumns(10);
		
		JLabel label_2 = new JLabel(">");
		label_2.setBounds(553, 509, 21, 14);
		contentPane.add(label_2);
		
		JLabel lblNewLabel_8 = new JLabel("PRODUCT FILTER");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_8.setBounds(507, 539, 98, 14);
		contentPane.add(lblNewLabel_8);
		
		JButton btnPriceFilter = new JButton("PRICE FILTER");
		btnPriceFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				ProductDAO prdao=new ProductDAO();
				for(int i=model2.getRowCount()-1;i>=0;i--)
					model2.removeRow(i);
				String s0=textField_16.getText();
				ArrayList<Product> products=(ArrayList<Product>) prdao.findAll();
				for(int i=0;i<products.size();i++){
					if(products.get(i).getPrice()<Double.parseDouble(s0)){
						product[0]=products.get(i).getId();
						product[1]=products.get(i).getPrice();
						product[2]=products.get(i).getName();
						product[3]=products.get(i).getQuantity();
						product[4]=products.get(i).getProducer();
						model2.addRow(product);
					}
				}
			}
		});
		btnPriceFilter.setBounds(432, 564, 119, 23);
		contentPane.add(btnPriceFilter);
		
		JLabel label_3 = new JLabel("<");
		label_3.setBounds(559, 568, 15, 14);
		contentPane.add(label_3);
		
		textField_16 = new JTextField();
		textField_16.setBounds(578, 565, 86, 20);
		contentPane.add(textField_16);
		textField_16.setColumns(10);
		
		JButton btnNewButton_13 = new JButton("GET RECEPIT");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO prdao=new ProductDAO();
				ClientDAO cldao=new ClientDAO();
				OrderDAO ordao=new OrderDAO();
				String s0=textField_17.getText();
				Order o=ordao.findById(Integer.parseInt(s0));
				Client c=cldao.findById(o.getClientId());
				Product p=prdao.findById(o.getProductId());
				
				try {
					PrintWriter out = new PrintWriter("receipt"+o.getId()+ ".txt");
					out.println("Client " + c.getName() + " which placed the order with the id " + o.getId() + " " + 
					"has to pay a total sum of:" + p.getPrice()*o.getQuantity() + " EURO.");
					out.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				}
				
				
			
		});
		btnNewButton_13.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton_13.setBounds(827, 480, 152, 64);
		contentPane.add(btnNewButton_13);
		
		JLabel lblOrder = new JLabel(" ORDER");
		lblOrder.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrder.setBounds(847, 555, 46, 14);
		contentPane.add(lblOrder);
		
		JLabel label_4 = new JLabel(" =>");
		label_4.setBounds(892, 555, 21, 14);
		contentPane.add(label_4);
		
		textField_17 = new JTextField();
		textField_17.setBounds(914, 552, 38, 20);
		contentPane.add(textField_17);
		textField_17.setColumns(10);
		
	}
}
