package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Order {
	//attributes of an order
	private int idOrder;
	private int orderQuantity;
	private int idClient;
	private int idProduct;
	//constructors
	public Order() {
	}

	public Order(int idOrder, int orderQuantity, int idClient, int idProduct) {
		super();
		this.idOrder = idOrder;
		this.orderQuantity = orderQuantity;
		this.idClient = idClient;
		this.idProduct = idProduct;
	}

	public Order(int orderQuantity, int idClient, int idProduct) {
		super();
		this.orderQuantity = orderQuantity;
		this.idClient = idClient;
		this.idProduct = idProduct;
	}
	//getters and setters
	public int getId() {
		return idOrder;
	}

	public void setId(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getQuantity() {
		return this.orderQuantity;
	}

	public void setQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public int getClientId() {
		return this.idClient;
	}

	public void setClientId(int idClient) {
		this.idClient = idClient;
	}

	public int getProductId() {
		return this.idProduct;
	}

	public void setProductId(int idProduct) {
		this.idProduct = idProduct;
	}



	@Override
	public String toString() {
		return "Student [idOrder=" + idOrder + ", orderQuantity=" + orderQuantity + ", idClient=" + idClient + ", idProduct=" + idProduct+"]";
	}

}