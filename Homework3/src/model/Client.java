package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Client {
	//atributes of a client
	private int idClient;
	private String clientName;
	private int clientAge;
	private int clientShoeSize;
	private String clientCountry;
	//constructors
	public Client() {
	}

	public Client(int idClient, String clientName, int clientAge, int clientShoeSize, String clientCountry) {
		super();
		this.idClient = idClient;
		this.clientName = clientName;
		this.clientAge = clientAge;
		this.clientShoeSize = clientShoeSize;
		this.clientCountry = clientCountry;
	}

	public Client(String clientName, int clientAge, int clientShoeSize, String clientCountry) {
		super();
		this.clientName = clientName;
		this.clientAge = clientAge;
		this.clientShoeSize = clientShoeSize;
		this.clientCountry = clientCountry;
	}
	//getters and setters
	public int getId() {
		return idClient;
	}

	public void setId(int idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return clientName;
	}

	public void setName(String clientName) {
		this.clientName = clientName;
	}

	public int getAge() {
		return clientAge;
	}

	public void setAge(int clientAge) {
		this.clientAge = clientAge;
	}

	public int getShoeSize() {
		return clientShoeSize;
	}

	public void setShoeSize(int clientShoeSize) {
		this.clientShoeSize = clientShoeSize;
	}

	public String getCountry() {
		return clientCountry;
	}

	public void setCountry(String clientCountry) {
		this.clientCountry = clientCountry;
	}

	@Override
	public String toString() {
		return "Student [idClient=" + idClient + ", clientName=" + clientName + ", clientAge=" + clientAge + ", clientShoeSize=" + clientShoeSize + ", clientCountry=" + clientCountry
				+ "]";
	}

}