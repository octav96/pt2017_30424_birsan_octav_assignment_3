package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Product {
	//attributes of a product
	private int idProduct;
	private double productPrice;
	private String productName;
	private int productQuantity;
	private String productProducer;
	//constructors
	public Product() {
	}

	public Product(int idProduct,double productPrice,String productName,  int productQuantity, String productProducer) {
		super();
		this.idProduct=idProduct;
		this.productPrice = productPrice;
		this.productName = productName;
		this. productQuantity =  productQuantity;
		this.productProducer = productProducer;
	}

	public Product(double productPrice,String productName,  int productQuantity, String productProducer) {
		super();
		this.productPrice = productPrice;
		this.productName = productName;
		this. productQuantity =  productQuantity;
		this.productProducer = productProducer;	}
	//getters and setters
	public int getId() {
		return idProduct;
	}

	public void setId(int idProduct) {
		this.idProduct = idProduct;
	}

	public double getPrice() {
		return productPrice;
	}

	public void setPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public String getName() {
		return productName;
	}

	public void setName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return productQuantity;
	}

	public void setQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public String getProducer() {
		return productProducer;
	}

	public void setProducer(String productProducer) {
		this.productProducer = productProducer;
	}

	@Override
	public String toString() {
		return "Student [idProduct=" + idProduct + ", productPrice=" + productPrice + ", productName=" + productName + ", productQuantity=" + productQuantity + ", productProducer=" + productProducer
				+ "]";
	}

}