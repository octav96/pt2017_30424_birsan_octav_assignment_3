package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (productPrice,productName,productQuantity,productProducer)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM product where idProduct = ?";
	private final static String findAllStatementString = "SELECT * FROM product";
	private final static String updateStatementString = "UPDATE product SET productPrice=?,productName=?,productQuantity=?,productProducer=? where idProduct = ?";
	private final static String deleteStatementString = "DELETE FROM product where idProduct = ?";
	//finding a product based on a given id
	public Product findById(int idProduct) {
		Product toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1,idProduct);
			rs = findStatement.executeQuery();
			//rs.next();
			if(rs.next()){
			Double productPrice = rs.getDouble("productPrice");
			String productName = rs.getString("productName");
			int productQuantity = rs.getInt("productQuantity");
			String productProducer = rs.getString("productProducer");
			
			toReturn = new Product(idProduct, productPrice, productName, productQuantity, productProducer);
			
			}else{
				return null;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	//finding all the products
	public List<Product> findAll(){
		List<Product> products=new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findAllStatement = null;
		ResultSet rs = null;
		try {
			findAllStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findAllStatement.executeQuery();
			
			if(rs.next()){
				double productPrice = rs.getDouble("productPrice");
				String productName = rs.getString("productName");
				int productQuantity = rs.getInt("productQuantity");
				String productProducer = rs.getString("productProducer");
				products.add(new Product(rs.getInt(1), productPrice, productName, productQuantity, productProducer));
				
			while(rs.next()){
				productPrice = rs.getDouble("productPrice");
				productName = rs.getString("productName");
				productQuantity = rs.getInt("productQuantity");
				productProducer = rs.getString("productProducer");
				products.add(new Product(rs.getInt(1), productPrice, productName, productQuantity, productProducer));
				
				}
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(dbConnection);
		}
		return products;
	}
	//updating a product based on a given id
	public Product updateProduct(int idProduct,double productPrice,String productName,  int productQuantity, String productProducer){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		ResultSet rs = null;
		Product p=findById(idProduct);
		if(p!=null){
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setDouble(1, productPrice);
			updateStatement.setString(2, productName);
			updateStatement.setInt(3, productQuantity);
			updateStatement.setString(4, productProducer);
			updateStatement.setInt(5,idProduct);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return p;
	}
	//deleting a product based on a given id
	public Product deleteProduct(int idProduct){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		Product p=findById(idProduct);
		if(p!=null){
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1,idProduct);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return p;
	}
		//inserting a product
		public int insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setDouble(1, product.getPrice());
			insertStatement.setString(2, product.getName());
			insertStatement.setInt(3, product.getQuantity());
			insertStatement.setString(4, product.getProducer());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}