package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Order;
import model.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class OrderDAO {
	//statements for performing the picked MySQL operations
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO `order` (orderQuantity,idClient,idProduct)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM `order` where idOrder = ?";
	private final static String findAllStatementString = "SELECT * FROM `order`";
	private final static String updateStatementString = "UPDATE `order` SET orderQuantity=?,idClient=?,idProduct=? where idOrder = ?";
	private final static String deleteStatementString = "DELETE FROM `order` where idOrder = ?";
	//finding an order based on a given id
	public Order findById(int idOrder) {
		Order toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1,idOrder);
			rs = findStatement.executeQuery();
			//rs.next();
			if(rs.next()){
			int orderQuantity = rs.getInt("orderQuantity");
			int idClient = rs.getInt("idClient");
			int idProduct = rs.getInt("idProduct");
			toReturn = new Order(idOrder, orderQuantity, idClient, idProduct);
			
			}else{
				return null;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	//finding all the orders
	public List<Order> findAll(){
		List<Order> orders=new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findAllStatement = null;
		ResultSet rs = null;
		try {
			findAllStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findAllStatement.executeQuery();
			
			if(rs.next()){
				int orderQuantity = rs.getInt("orderQuantity");
				int idClient = rs.getInt("idClient");
				int idProduct = rs.getInt("idProduct");
				//toReturn = new Order(idOrder, orderQuantity, idClient, idProduct);
				orders.add(new Order(rs.getInt(1), orderQuantity, idClient, idProduct));
				
			while(rs.next()){
				orderQuantity = rs.getInt("orderQuantity");
				idClient = rs.getInt("idClient");
				idProduct = rs.getInt("idProduct");
			//toReturn = new Order(idOrder, orderQuantity, idClient, idProduct);
				orders.add(new Order(rs.getInt(1), orderQuantity, idClient, idProduct));
				
				}
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(dbConnection);
		}
		return orders;
	}
	//updating an order based on a given id
	public Order updateOrder(int idOrder, int orderQuantity, int idClient, int idProduct){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		ResultSet rs = null;
		Order o=findById(idOrder);
		if(o!=null){
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1,orderQuantity);
			updateStatement.setInt(2, idClient);
			updateStatement.setInt(3, idProduct);
			updateStatement.setInt(4,idOrder);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return o;
	}
	//deleting an order based on a given id
	public Order deleteOrder(int idOrder){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		Order o=findById(idOrder);
		if(o!=null){
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1,idOrder);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return o;
	}
		//inserting an order
		public int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet rs= null;
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getQuantity());
			insertStatement.setInt(2, order.getClientId());
			insertStatement.setInt(3, order.getProductId());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(rs);
		}
		return insertedId;
	}
}