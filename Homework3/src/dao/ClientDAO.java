package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientDAO {
	//statements for performing the picked MySQL operations
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (clientName,clientAge,clientShoeSize,clientCountry)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM client where idClient = ?";
	private final static String findAllStatementString = "SELECT * FROM client";
	private final static String updateStatementString = "UPDATE client SET clientName=?,clientAge=?,clientShoeSize=?,clientCountry=? where idClient = ?";
	private final static String deleteStatementString = "DELETE FROM client where idClient = ?";
	//finding a client by its id 
	public Client findById(int idClient) {
		Client toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection(); 
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1,idClient);
			rs = findStatement.executeQuery();

			if(rs.next()){
			String clientName = rs.getString("clientName");
			int clientAge = rs.getInt("clientAge");
			int clientShoeSize = rs.getInt("clientShoeSize");
			String clientCountry = rs.getString("clientCountry");
			toReturn = new Client(idClient, clientName, clientAge, clientShoeSize, clientCountry);
			}else{
				return null;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	//finding all clients
	public List<Client> findAll(){
		List<Client> clients=new ArrayList<Client>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findAllStatement = null;
		ResultSet rs = null;
		try {
			findAllStatement = dbConnection.prepareStatement(findAllStatementString);
			//findStatement.setInt(1,idClient);
			rs = findAllStatement.executeQuery();
			//rs.next();
			if(rs.next()){
				String clientName = rs.getString("clientName");
				int clientAge = rs.getInt("clientAge");
				int clientShoeSize = rs.getInt("clientShoeSize");
				String clientCountry = rs.getString("clientCountry");
				clients.add(new Client(rs.getInt(1), clientName, clientAge, clientShoeSize, clientCountry));
				
			while(rs.next()){
				clientName = rs.getString("clientName");
				clientAge = rs.getInt("clientAge");
				clientShoeSize = rs.getInt("clientShoeSize");
				clientCountry = rs.getString("clientCountry");
				clients.add(new Client(rs.getInt(1), clientName, clientAge, clientShoeSize, clientCountry));
			}
			}
			} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(dbConnection);
		}
		return clients;
	}
	//updating a client based on a given id
	public Client updateClient(int idClient,String clientName,int clientAge,int clientShoeSize,String clientCountry){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		ResultSet rs = null;
		Client c=findById(idClient);
		if(c!=null){
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, clientName);
			updateStatement.setInt(2, clientAge);
			updateStatement.setInt(3, clientShoeSize);
			updateStatement.setString(4, clientCountry);
			updateStatement.setInt(5,idClient);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return c;
	}
	//deleting a client based on a given id
	public Client deleteClient(int idClient){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		Client c=findById(idClient);
		if(c!=null){
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1,idClient);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		return c;
	}
		//inserting a client
		public int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setInt(2, client.getAge());
			insertStatement.setInt(3, client.getShoeSize());
			insertStatement.setString(4, client.getCountry());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}