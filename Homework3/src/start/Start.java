package start;

import java.awt.EventQueue;
//import java.awt.ArrayList;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import gui.Interface;
import model.Client;
import model.Order;
import model.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {
		//instatiation of a GUI created with WindowBuilder
		Interface frame = new Interface();
		frame.setVisible(true);

	}

}